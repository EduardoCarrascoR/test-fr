# TestFr

It is a project which creates the data, brings the data by pagination and removes from the pagination of the data

## Tech

TestFr was originally carried out with:

- [NestJs] - evented I/O for the backend.
- [MongoDB] - is a non-relational database.


## Installation

TestFr requires [Node.js](https://nodejs.org/) v16.13.1 to run.

Install the dependencies and devDependencies and start the server.
It must be in the rais folder of the project.

```sh
npm i
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

## Packages

TestFr contains all the relevant packages.
Instructions on how to use them in your own application are linked below.

| Package | link
| ------ | ------ |
| Passport | [https://www.npmjs.com/package/passport][PlDb] |
| Passport-jwt | [https://www.npmjs.com/package/passport-jwt][PlGh] |
| Passport-local | [https://www.npmjs.com/package/passport-local][PlGd] |
| Swagger-ui-express | [https://www.npmjs.com/package/swagger-ui-express][PlOd] |
| @nestjs/swagger | [https://www.npmjs.com/package/@nestjs/swagger][PlOd] |
| Mongoose | [https://www.npmjs.com/package/mongoose][PlMe] |
| @nestjs/mongoose | [https://www.npmjs.com/package/@nestjs/mongoose][PlMe] |
| @nestjs/passport | [https://www.npmjs.com/package/@nestjs/passport][PlMe] |
| dayjs | [https://www.npmjs.com/package/dayjs][PlGa] |
| class-validator | [https://www.npmjs.com/package/class-validator][PlGa] |
| class-transformer | [https://www.npmjs.com/package/class-transformer][PlGa] |
| Bcrypt | [https://www.npmjs.com/package/bcrypt][PlGa] |
| @nestjs/jwt | [https://www.npmjs.com/package/@nestjs/jwt][PlGa] |


## Docker
To avoid uploading unnecessary files and problems with bcrypt, the file is created:
```sh
.git
.ipynb_checkpoints/*
/notebooks/*
/node_modules/*
/coverage/*
/unused/*
Dockerfile
.DS_Store
.gitignore
README.md
/devops/*
```

To dokerize the project an image is created which is composed of the project, and additionally the Dockerfile file is created.

```sh
FROM node:16.13.1
# Create app directory
WORKDIR /test-fr/src/app
 
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
COPY .env ./
 
RUN npm install 

# If you are building your code for production
# RUN npm ci --only=production
COPY . .
 
EXPOSE 3000
CMD [ "node", "dist/src/main.js" ]
```


run container image test-fr.

```sh
docker build -t test-fr:v1.
```
Now to create the database with mongo, create a docker-compose.yml with the following data:
```sh
version: '3.8'

services:
    mongo:
      image: mongo:latest
      volumes:
        - ./challengeDBT:/var/lib/challengeBDT
      ports:
          - 27017:27017
      environment:
        MONGO_INITDB_DATABASE: challengeBDT

    dev:
        container_name: test-fr-app
        image: test-fr:v1
        ports:
            - 3000:3000
        environment:
          MONGODB_URI: mongodb://mongo:27017/challengeBDT
          JWT_SECRET: SecretTReign
          DEFAULT_USER_EMAIL: Username@email.com
          DEFAULT_USER_PASSWORD: pass
          PORT: 3000
        depends_on:
          - mongodb-myapp


```


This will create the dillinger image and pull in the necessary dependencies.
Be sure to swap out `${package.json.version}` with the actual
version of Dillinger.

Once done, run the Docker image and map the port to whatever you wish on
your host. In this example, we simply map port 8000 of the host to
port 8080 of the Docker (or whatever port was exposed in the Dockerfile):

```sh
docker run -d -p 8000:8080 --restart=always --cap-add=SYS_ADMIN --name=dillinger <youruser>/dillinger:${package.json.version}
```

> Note: `--capt-add=SYS-ADMIN` 

Verify the deployment by navigating to your server address in
your preferred browser.

```sh
http://localhost:3000
```
## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation dependencies

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Information

- Author - Eduardo Carrasco Romero
- GitHub - [EduardoCarrascoR](https://github.com/EduardoCarrascoR)

## License

TestFr is [MIT licensed](LICENSE).