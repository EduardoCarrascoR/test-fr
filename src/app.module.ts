import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HitsModule } from './modules/Hits/hits.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MONGODB_URI } from './config/constants';
import { HttpModule } from '@nestjs/axios';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './modules/users/users.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>(MONGODB_URI),
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }),
      inject: [ConfigService],
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env'
    }),
    HitsModule, HttpModule, AuthModule,  UsersModule],
  controllers: [],
})
export class AppModule {}
