import { ConfigService } from "@nestjs/config";
import { connect, Mongoose} from 'mongoose';
import { UserSchema } from "src/schemas";
import { DEFAULT_USER_EMAIL, DEFAULT_USER_PASSWORD, MONGODB_URI } from "./constants";

export const setDefaultUser = async (config: ConfigService) =>{
    
    const db = await connect(config.get<string>(MONGODB_URI))
    const session = await db.startSession()
    const Usermodel = db.model('User', UserSchema)

    await session.withTransaction( async () => {
        const defaultUser = await Usermodel.findOne({ email: config.get<string>(DEFAULT_USER_EMAIL) })

        if(!defaultUser) {
            const adminUser = Usermodel.create({
                email: config.get<string>(DEFAULT_USER_EMAIL),
                password: config.get<string>(DEFAULT_USER_PASSWORD)
            })
    
            return await adminUser
        }
    });
}
