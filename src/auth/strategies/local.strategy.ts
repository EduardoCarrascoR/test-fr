import { HttpException, HttpStatus, Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { AuthService } from "../services/auth.service";

@Injectable()
export class localStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly authService: AuthService
    ) {
        super({
            usernameField: 'email',     //email user
            passwordField: 'password'   //passport
        })
    }
    async validate( email: string, password: string) {
        const user = await this.authService.validateUserAdmin(email, password)

        if(!user) throw new HttpException({ success: false, status: HttpStatus.NOT_FOUND, message: 'Login user or password does not match'}, HttpStatus.NOT_FOUND);
        return user;
    }
}