import { Body, Controller, Get, HttpStatus, Post, Res, UseGuards } from '@nestjs/common';
import { Auth, User } from 'src/common/decorators';
import { localAuthGuard } from 'src/guards';
import { UserDocument } from 'src/schemas';
import { loginDto } from '../dto/auth';
import { AuthService } from '../services/auth.service';

@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService
    ){}
  
    @Post('/login')
    async loginWeb(@Body() loginDto: loginDto, @User() user: UserDocument, @Res() res){
      const data =  await this.authService.login(loginDto)
      return res.status(HttpStatus.ACCEPTED).json({ success: true, message: 'Login success', data })
    }
  
    @Auth()
    @Get('/profile')
    profile(@User() user: UserDocument, @Res() res){
      return res.status(HttpStatus.OK).json({ success: true, message: 'Peticion correcta', user })
    }
  
    @Auth()
    @Get('/refresh')
    async refreshToken(@Body() loginDto: loginDto, @User() user: UserDocument, @Res() res){
      const data =  await this.authService.login(loginDto)
      return res.status(HttpStatus.ACCEPTED).json({ success: true, message: 'Fefresh sucess', data })
    }
  
}
