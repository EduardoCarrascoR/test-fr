import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from "bcrypt";
import { UsersService } from '../../modules/users/services/users.service';
import { loginDto } from '../dto/auth';

@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UsersService,
        private readonly jwtService: JwtService
    ){}
    

    async validateUserAdmin(email: string, pass: string): Promise<any> {
        const user = await this.userService.findOneUserByEmail(email);

        if(user) {
            if(bcrypt.compareSync(pass, user.password)) {        
            const { password, ...rest } = user

            return rest;
            } else {
                throw new HttpException({ success: false, status: HttpStatus.BAD_REQUEST, message: "User doesn't exist or authorized"},HttpStatus.BAD_REQUEST);

            }
        }
    }

    async login(user: loginDto) {
        const { email, ...rest } = user;
        const userDB =await this.userService.findOneUserByEmail(email);
        const payload = { sub: userDB._id };

        return {
            accessToken: this.jwtService.sign(payload)

        }
    }   
}
