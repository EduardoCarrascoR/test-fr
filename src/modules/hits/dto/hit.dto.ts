import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsUrl, IsDateString, IsNumber, IsBoolean, Max, Min, IsEmpty, isEmpty, IsOptional, IsMongoId, IsEnum } from 'class-validator';
import { ObjectId } from 'mongoose';
import { monthType } from '../../../common/enums/month-types.enum';
import { EnumToString } from '../../../common/helpers/enumToString';


export class HitsPaginationDTO {
    
    @IsNumber()
    @ApiProperty()
    @Min(1,{ message:  'The page number cannot be less than 1, 🤡' })
    page: number;

    @IsNumber()
    @Min(1,{ message:  'The limit number is less than 1' })
    @Max(5,{ message:  'The number exceeds 5' })
    @ApiProperty()
    limit: number;

    @IsString()
    @ApiProperty()
    @IsOptional()
    author?: string;

    @IsString()
    @ApiProperty()
    @IsOptional()
    title?: string;

    @IsString()
    @ApiProperty()
    @IsOptional()
    _tag?: string[];

    @IsString()
    @ApiProperty()
    @IsEnum(monthType,{ 
        message: `must be a valid role value: ${ EnumToString(monthType) }.`
    })
    @IsOptional()
    month?: string;

}

export class HitsRemoveDTO {
    
    @IsMongoId()
    @ApiProperty()
    _id: string;

}




