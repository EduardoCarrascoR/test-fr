import { Body, Controller, Delete, Get, HttpException, HttpStatus, Patch, Post, Res } from '@nestjs/common';
import { Auth } from '../../../common/decorators';
import { HitsPaginationDTO, HitsRemoveDTO } from '../dto/hit.dto';
import { HitsService } from '../services/hits.service';

@Controller('hits')
export class HitsController {
    constructor(
        private readonly hitsService: HitsService
    ) {}

    @Auth()
    @Get('/createData')
    async createDatainDB(@Res() res) {
        const data = await this.hitsService.findHits();

        if(!data) throw new HttpException({ success: false, status: HttpStatus.OK, message: "Hits created." }, HttpStatus.OK)
        return res.status(HttpStatus.OK).json({ success: true, data: data })
    }

    @Auth()
    @Get('/all')
    async findAll(@Res() res?) {
        const hits = await this.hitsService.findAllHitsDB();

        if(!hits) throw new HttpException({ success: false, status: HttpStatus.OK, message: "Hits not found." }, HttpStatus.OK)
        return res.status(HttpStatus.OK).json({ success: true, hits })
    }

    @Auth()
    @Post('/paginationHits')
    async HitsPagination(@Body() paginationDTO: HitsPaginationDTO, @Res() res?) {
        
        const hits = await this.hitsService.getHitWithPagination(paginationDTO);
        if(!hits) throw new HttpException({ success: false, status: HttpStatus.OK, message: "Hits not found." }, HttpStatus.OK)
        if(!hits.totalPages) throw new HttpException({ success: false, status: HttpStatus.BAD_REQUEST, message: "Hits not found because the page limit is exceeded." }, HttpStatus.BAD_REQUEST)
        if(hits.hitsFiltered == null) throw new HttpException({ success: false, status: HttpStatus.BAD_REQUEST, message: "Hits data not found or Hits not found because the page limit is exceeded." }, HttpStatus.BAD_REQUEST)
        
        return res.status(HttpStatus.ACCEPTED).json({ success: true, message: `Hits page ${hits.pageSelected}.`, totalPages: hits.totalPages, hits: hits.hitsFiltered })
    }

    @Auth()
    @Patch('/deleteHit')
    async deletetHit(@Body() hitsRemove: HitsRemoveDTO, @Res() res) {
        
        const hit = await this.hitsService.deleteHit( hitsRemove._id );
        if(!hit) throw new HttpException({ success: false, status: HttpStatus.BAD_REQUEST, message: "Hit not found." }, HttpStatus.BAD_REQUEST)
         
        return res.status(HttpStatus.ACCEPTED).json({ success: true, message: `Hit found and remove.`, hit })
    }
}