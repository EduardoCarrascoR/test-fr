import { HttpModule, HttpService } from '@nestjs/axios';
import { BadRequestException } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Author, Comment_text, HighlightResult, Hit, Story_title, Story_url } from '../../../schemas';
import { HitsPaginationDTO, HitsRemoveDTO } from '../dto/hit.dto';
import { HitsService } from '../services/hits.service';
import { HitsController } from './hits.controller';

describe('HitsController', () => {
  let controller: HitsController;
  let hitMockService: HitsService;
  let httpService: {};

  let hitsPaginationDTO = new HitsPaginationDTO();
  (hitsPaginationDTO.page = 1),(hitsPaginationDTO.limit = 5), (hitsPaginationDTO.author = "all2");

  const hitservice = {
    post: jest.fn((hit) => {
      return {
        "success": true,
        "message": "Hits page 1.",
        "totalPages": 1,
        "hits": [
          {
              "_id": "61be544eaf3f6e52e68930f3",
              "objectID": 29608214,
              "_tags": [
                  "comment",
                  "author_all2",
                  "story_29606591"
              ],
              "created_at_i": 1639859365,
              "parent_id": 29607940,
              "story_url": "https://www.reuters.com/world/un-talks-adjourn-without-deal-regulate-killer-robots-2021-12-17/",
              "story_title": "U.N. talks adjourn without deal to regulate 'killer robots'",
              "story_id": 29606591,
              "num_comments": null,
              "comment_text": " nostalgia. Nostalgia does not guarantee me personal autonomy. Neither does it guarantee me a right to self defense. Nor does it guarantee a free practice of religion, free press, or a right of association. Ensuring the survival of a nation state that adheres to the US Constitution will guarantee those things. How long will the internet abide states as nodes of economic and social standing? Take a look at China and their authoritarian take on the internet. The internet, that is the masses that make it up, do what they're told, consume what they are allowed to consume, demonize those they are told they should demonize. Goebbels would be jealous of the machine the modern world has developed.",
              "story_text": "null",
              "points": null,
              "author": "all2",
              "url": null,
              "title": null,
              "created_at": "2021-12-18T20:29:25.000Z",
              "__v": 0,
              "remove": false
          }
        ]
      }
    })
  }

  const mockHit = {}, 
    mockHighlightResult = {}, 
    mockAuthor = {}, 
    mockComment_text = {}, 
    mockStory_title = {}, 
    mockStory_url = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        HitsController,
        HitsService,
        { provide: HitsService, useValue: HitsService },
        { provide: HttpService, useValue: httpService },
        { provide: getModelToken(Hit.name), useValue: mockHit },
        { provide: getModelToken(HighlightResult.name), useValue: mockHighlightResult },
        { provide: getModelToken(Author.name), useValue: mockAuthor },
        { provide: getModelToken(Comment_text.name), useValue: mockComment_text },
        { provide: getModelToken(Story_title.name), useValue: mockStory_title },
        { provide: getModelToken(Story_url.name), useValue: mockStory_url },
      ]
    }).compile();

    controller = module.get<HitsController>(HitsController);
    hitMockService = module.get<HitsService>(HitsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should have HitsPagination function', () => {
    expect(controller.HitsPagination).toBeDefined();
  });

  it('should have deletetHit function', () => {
    expect(controller.deletetHit).toBeDefined();
  });

  it('should have findAll function', () => {
    expect(controller.findAll).toBeDefined();
  });

  /* it('should find Pagination', async () => {
    expect(controller.HitsPagination(hitsPaginationDTO)).toEqual({
      "success": true,
      "message": "Hits page 1.",
      "totalPages": 1,
      "hits": [
        {
            "_id": "61be544eaf3f6e52e68930f3",
            "objectID": 29608214,
            "_tags": [
                "comment",
                "author_all2",
                "story_29606591"
            ],
            "created_at_i": 1639859365,
            "parent_id": 29607940,
            "story_url": "https://www.reuters.com/world/un-talks-adjourn-without-deal-regulate-killer-robots-2021-12-17/",
            "story_title": "U.N. talks adjourn without deal to regulate 'killer robots'",
            "story_id": 29606591,
            "num_comments": null,
            "comment_text": " nostalgia. Nostalgia does not guarantee me personal autonomy. Neither does it guarantee me a right to self defense. Nor does it guarantee a free practice of religion, free press, or a right of association. Ensuring the survival of a nation state that adheres to the US Constitution will guarantee those things. How long will the internet abide states as nodes of economic and social standing? Take a look at China and their authoritarian take on the internet. The internet, that is the masses that make it up, do what they're told, consume what they are allowed to consume, demonize those they are told they should demonize. Goebbels would be jealous of the machine the modern world has developed.",
            "story_text": "null",
            "points": null,
            "author": "all2",
            "url": null,
            "title": null,
            "created_at": "2021-12-18T20:29:25.000Z",
            "__v": 0,
            "remove": false
        }
      ]
    });
    expect(hitMockService.getHitWithPagination).toBeCalled();
  }); */


});
