import { Module } from '@nestjs/common';
import { HitsService } from './services/hits.service';
import { HitsController } from './controllers/hits.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HitSchema, Hit, HighlightResultSchema, HighlightResult, AuthorSchema, Author, Comment_textSchema, Comment_text, Story_titleSchema, Story_title, Story_urlSchema, Story_url } from '../../schemas';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Hit.name, schema: HitSchema },
      { name: HighlightResult.name, schema: HighlightResultSchema },
      { name: Author.name, schema: AuthorSchema },
      { name: Comment_text.name, schema: Comment_textSchema },
      { name: Story_title.name, schema: Story_titleSchema },
      { name: Story_url.name, schema: Story_urlSchema },
    ]),
    HttpModule
  ],
  providers: [HitsService],
  controllers: [HitsController]
})
export class HitsModule {}
