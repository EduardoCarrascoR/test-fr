import { HttpService } from '@nestjs/axios';
import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Author, AuthorDocument, Comment_text, Comment_textDocument, HighlightResult, HighlightResultDocument, Hit, HitDocument, Story_title, Story_titleDocument, Story_url, Story_urlDocument } from '../../../schemas';
import { HitsPaginationDTO, HitsRemoveDTO } from '../dto/hit.dto';
import * as dayjs from "dayjs";


@Injectable()
export class HitsService {

    constructor(
        private httpService: HttpService,
        @InjectModel(Hit.name) private HitModel: Model<HitDocument>,
        @InjectModel(HighlightResult.name) private HighlightResultModel: Model<HighlightResultDocument>,
        @InjectModel(Author.name) private AuthorModel: Model<AuthorDocument>,
        @InjectModel(Comment_text.name) private Comment_textModel: Model<Comment_textDocument>,
        @InjectModel(Story_title.name) private Story_titleModel: Model<Story_titleDocument>,
        @InjectModel(Story_url.name) private Story_urlModel: Model<Story_urlDocument>,

    ) {}

    async findHits() {
        let TitleId, UrlId, AuthorId, Comment_textId, Story_titleId, Story_urlId, HighlightResultId
        const response = await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').toPromise()
        
        for (const data of response.data.hits) {

            const defaultData =  await this.HitModel.findOne({ objectID: data.objectID });
            
            if(!defaultData) {
                await this.DataCorrection(data);
        
                if (data._highlightResult.author !== undefined) {
                    const createdAuthor = await new this.AuthorModel({
                        value: data._highlightResult.author.value,
                        matchLevel: data._highlightResult.author.matchLevel,
                        fullyHighlighted: data._highlightResult.author.fullyHighlighted,
                        matchedWords: data._highlightResult.author.matchedWords
                    })
                    await createdAuthor.save( (err,author) => {
                        AuthorId= author._id
                    })
                } else AuthorId = null;
                if (data._highlightResult.comment_text !== undefined) {
                    const createdComment_text = await new this.Comment_textModel({
                        value: data._highlightResult.comment_text.value,
                        matchLevel: data._highlightResult.comment_text.matchLevel,
                        fullyHighlighted: data._highlightResult.comment_text.fullyHighlighted,
                        matchedWords: data._highlightResult.comment_text.matchedWords
                    })
                    await createdComment_text.save( (err,comment_text) => {
                        Comment_textId= comment_text._id
                    })
                } else Comment_textId = null;
                if (data._highlightResult.story_title !== undefined) {
                    const createdStory_title = await new this.Story_titleModel({
                        value: data._highlightResult.story_title.value,
                        matchLevel: data._highlightResult.story_title.matchLevel,
                        fullyHighlighted: data._highlightResult.story_title.fullyHighlighted,
                        matchedWords: data._highlightResult.story_title.matchedWords
                    })
                    await createdStory_title.save( (err,story_title) => {
                        Story_titleId= story_title._id
                    })
                } else Story_titleId = null;
                if (data._highlightResult.story_url !== undefined) {
                    const createdStory_url = await new this.Story_urlModel({
                        value: data._highlightResult.story_url.value,
                        matchLevel: data._highlightResult.story_url.matchLevel,
                        fullyHighlighted: data._highlightResult.story_url.fullyHighlighted,
                        matchedWords: data._highlightResult.story_url.matchedWords
                    })
                    await createdStory_url.save( (err,story_url) => {
                        Story_urlId= story_url._id
                    })
                } else Story_urlId = null;

                const createdHighlightResult = await new this.HighlightResultModel({
                    title: TitleId,
                    url: UrlId,
                    author: AuthorId,
                    comment_text: Comment_textId,
                    story_title: Story_titleId,
                    story_url: Story_urlId
                })
                await createdHighlightResult.save( (err,_highlightResult) => {
                    HighlightResultId= _highlightResult._id
                })
                const createdHits = new this.HitModel({
                    created_at: new Date(data.created_at),
                    title: data.title,
                    url: data.url,
                    author: data.author,
                    points: data.points,
                    story_text: data.story_text,
                    comment_text: data.comment_text,
                    num_comments: data.num_comments,
                    story_id: data. story_id,
                    story_title: data.story_title,
                    story_url: data.story_url,
                    parent_id: data.parent_id,
                    created_at_i: data.created_at_i,
                    _tags: data._tags,
                    objectID: data.objectID,
                    _highlightResult: HighlightResultId
                })

                await createdHits.save();
            }
        }

        return response.data.hits
    }
    async findAllHitsDB() {
        const hits = this.HitModel.find({ remove : false })

        return hits
    }

    async getHitWithPagination(hitsPagination: HitsPaginationDTO){
        
        let hitsFiltered = await this.HitModel.find({ remove: false })

        let originalArray = await this.dataFilter(hitsFiltered, hitsPagination)
        let totalPages = Math.ceil(originalArray.length/hitsPagination.limit)
        let offset = hitsPagination.limit * (hitsPagination.page - 1)
        let newArray = []

        for (let i = 0; i < hitsPagination.limit; i++) {
        newArray.push(originalArray[offset+i])
        if(offset+i+2 > originalArray.length){
            break;
        }
        }

        if (!newArray) throw new HttpException({ success: false, status: HttpStatus.OK, message: 'Hits does not exists or unauthorized.'}, HttpStatus.OK)
        if (totalPages < hitsPagination.page) throw new HttpException({ success: false, status: HttpStatus.NOT_ACCEPTABLE, message: `Hits doesn't exists or Hits not found because the page limit is exceeded.`}, HttpStatus.NOT_ACCEPTABLE)
        
        return await { totalPages, pageSelected: hitsPagination.page, hitsFiltered: newArray } 
    }

    async deleteHit(_id: string) {

        const removeHit = await this.HitModel.findByIdAndUpdate({ _id},{ remove: true})

        return removeHit
    }

    async DataCorrection(contenido){
        
        let story_text = JSON.stringify(contenido.story_text).replace(/\"/g, '').replace(/<[^>]+>/g, ' ').replace(/&#x27;/g,"'").replace(/&#x2F;/g,"/").replace(/ \//g, ' ').replace(/&quot;/g, '').replace(/&gt;/g, '').replace(/&lt;/g, '').replace(/ \* /g, '; ').replace(/:;/g, ': ').replace(/\\n/g, ': ').replace(/------------------------------------- /g, '').replace(/  /g, ' ');
        contenido.story_text= story_text;
        
        let comment_text = JSON.stringify(contenido.comment_text).replace(/\"/g, '').replace(/<[^>]+>/g, ' ').replace(/&#x27;/g,"'").replace(/&#x2F;/g,"/").replace(/ \//g, ' ').replace(/&quot;/g, '').replace(/&gt;/g, '').replace(/&lt;/g, '').replace(/ \* /g, '; ').replace(/:;/g, ': ').replace(/;\) /g, '').replace(/:\) /g, '').replace(/\\n/g, ': ').replace(/  /g, ' ');
        contenido.comment_text= comment_text;

        if (contenido._highlightResult.comment_text !== undefined) {
            
            let datas = await JSON.stringify(contenido._highlightResult.comment_text.value).replace(/\"/g, '').replace(/<[^>]+>/g, ' ').replace(/ \//g, ' ').replace(/&quot;/g, '').replace(/&gt;/g, '').replace(/&lt;/g, '').replace(/ \* /g, '; ').replace(/:;/g, ': ').replace(/;\) /g, '').replace(/:\) /g, '').replace(/\\n/g, ': ').replace(/  /g, ' ')
            contenido._highlightResult.comment_text.value= datas
        }
        if (contenido._highlightResult.story_text !== undefined) {
            
            let story_text = await JSON.stringify(contenido._highlightResult.story_text.value).replace(/\"/g, '').replace(/<[^>]+>/g, ' ').replace(/ \//g, ' ').replace(/&quot;/g, '').replace(/&gt;/g, '').replace(/&lt;/g, '').replace(/ \* /g, '; ').replace(/:;/g, ': ').replace(/\\n/g, ': ').replace(/------------------------------------- /g, '').replace(/  /g, ' ')
            contenido._highlightResult.story_text.value= story_text
        }

        return  contenido
    }

    async dataFilter(hitsFiltered, hitsPagination){

        if (hitsPagination.title !== undefined ) hitsFiltered = await hitsFiltered.filter((hit) => {
            if (hit.title !== null) {
                
                if (hit.title === hitsPagination.title) return hit;
            } else {

                if (hit.story_title === hitsPagination.title) return hit;
            }
        })
        if (hitsPagination.author !== undefined ) hitsFiltered = await hitsFiltered.filter((hit) => hit.author == hitsPagination.author)
        if (hitsPagination.month) hitsFiltered = await hitsFiltered.filter(hit => dayjs(hit.created_at).format('MMMM') === hitsPagination.month)
        if (hitsPagination._tags?.length > 0) hitsFiltered = hitsFiltered.filter(hit => {
            return hitsPagination._tags.every(tag => {
              return hit._tags.includes(tag)
            })
          })

        return await hitsFiltered
    }
}
