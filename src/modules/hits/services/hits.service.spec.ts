import { Test, TestingModule } from '@nestjs/testing';
import { HitsService } from './hits.service';
import { HttpModule, HttpService } from '@nestjs/axios';
import { HitsModule } from '../hits.module';
import { getModelToken } from '@nestjs/mongoose';
import { Author, Comment_text, HighlightResult, Hit, Story_title, Story_url } from '../../../schemas';
import { HitsPaginationDTO } from '../dto/hit.dto';


const mockHit = {
    
  }, 
  mockHighlightResult = {
    
  }, 
  mockAuthor = {
    
  }, 
  mockComment_text = {
    
  }, 
  mockStory_title = {
    
  }, 
  mockStory_url = {
    
  };

describe('HitsService', () => {
  let service: HitsService;
  let httpService: {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        HttpModule,
      ],
      providers: [
        HitsService,
        { provide: HitsService, useValue: HitsService },
        { provide: HttpService, useValue: httpService },
        { provide: getModelToken(Hit.name), useValue: mockHit },
        { provide: getModelToken(HighlightResult.name), useValue: mockHighlightResult },
        { provide: getModelToken(Author.name), useValue: mockAuthor },
        { provide: getModelToken(Comment_text.name), useValue: mockComment_text },
        { provide: getModelToken(Story_title.name), useValue: mockStory_title },
        { provide: getModelToken(Story_url.name), useValue: mockStory_url },
      ],
    }).compile();

    service = module.get<HitsService>(HitsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  /* it('calling deletetHit function', async () => {
    const deleteHitSpy = jest.spyOn(service, 'deleteHit');
    const _id = 'noteId';
    service.deleteHit(_id);
    expect(deleteHitSpy).toHaveBeenCalledWith(_id);
  }); */
});
