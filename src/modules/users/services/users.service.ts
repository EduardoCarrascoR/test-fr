import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/schemas';

@Injectable()
export class UsersService {

    constructor(
        @InjectModel(User.name) private userModel: Model<UserDocument>
    ){}


    async findOneUser(_id: string, userEntity?: UserDocument) {
        const user = await this.userModel.findOne({_id})
        .then(u => !userEntity ? u : !!u && userEntity._id === u._id ? u : null)

        if (!user) throw new HttpException({ success: false, status: HttpStatus.BAD_REQUEST, message: 'User does not exists or unauthorized'}, HttpStatus.BAD_REQUEST)
        
        return user;
    }

    async findOneUserByEmail(email: string, userEntity?: UserDocument) {
        const user = await this.userModel.findOne({email})
        .then(u => !userEntity ? u : !!u && userEntity._id === u._id ? u : null)

        if (!user) throw new HttpException({ success: false, status: HttpStatus.BAD_REQUEST, message: 'User does not exists or unauthorized'}, HttpStatus.BAD_REQUEST)
        
        return user;
    }
}
