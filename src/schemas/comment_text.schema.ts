import { Prop,  Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export type Comment_textDocument = Comment_text & Document;

@Schema()
export class Comment_text {
  @Prop()
  value: String;

  @Prop()
  matchLevel: String;

  @Prop()
  fullyHighlighted: Boolean;

  @Prop()
  matchedWords: [String];
}

export const Comment_textSchema = SchemaFactory.createForClass(Comment_text);