import { Prop,  Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { HighlightResult } from './_highlightResult.schema';
/* import { HighlightResult } from './_highlightResult.schema'; */

export type HitDocument = Hit & Document;

@Schema()
export class Hit {
  @Prop()
  created_at: Date;

  @Prop()
  title: String;

  @Prop()
  url: String;

  @Prop()
  author: String;

  @Prop()
  points: Number;

  @Prop()
  story_text: String;

  @Prop()
  comment_text: String;

  @Prop()
  num_comments: Number;

  @Prop()
  story_id: Number;

  @Prop()
  story_title: String;

  @Prop()
  story_url: String;

  @Prop()
  parent_id: Number;

  @Prop()
  created_at_i: Number;

  @Prop()
  _tags: [String];

  @Prop()
  objectID: Number;

  @Prop({ default: false})
  remove: Boolean;

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'HighlightResult' })
  _highlightResult: HighlightResult;
}

export const HitSchema = SchemaFactory.createForClass(Hit);