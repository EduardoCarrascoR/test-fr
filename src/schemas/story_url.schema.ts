import { Prop,  Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export type Story_urlDocument = Story_url & Document;

@Schema()
export class Story_url {
  @Prop()
  value: String;

  @Prop()
  matchLevel: String;

  @Prop()
  matchedWords: [String];
}

export const Story_urlSchema = SchemaFactory.createForClass(Story_url);