import { Prop,  Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export type Story_titleDocument = Story_title & Document;

@Schema()
export class Story_title {
  @Prop()
  value: String;

  @Prop()
  matchLevel: String;

  @Prop()
  matchedWords: [String];
}

export const Story_titleSchema = SchemaFactory.createForClass(Story_title);