import { Prop,  Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export type AuthorDocument = Author & Document;

@Schema()
export class Author {
  @Prop()
  value: String;

  @Prop()
  matchLevel: String;

  @Prop()
  matchedWords: [String];
}

export const AuthorSchema = SchemaFactory.createForClass(Author);