import { Prop,  Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Author } from './author.schema';
import { Comment_text } from './comment_text.schema';
import { Story_title } from './story_title.schema';
import { Story_url } from './story_url.schema';

export type HighlightResultDocument = HighlightResult & Document;

@Schema()
export class HighlightResult {

    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'Author' })
    author: Author;

    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'Comment_text' })
    comment_text: Comment_text;

    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'Story_title' })
    story_title: Story_title;

    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'Story_url' })
    story_url: Story_url;
}

export const HighlightResultSchema = SchemaFactory.createForClass(HighlightResult);