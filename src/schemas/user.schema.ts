import { Prop,  Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop()
  email: String;

  @Prop()
  firtName: String;

  @Prop()
  password: String;
}

export const UserSchema = SchemaFactory.createForClass(User);