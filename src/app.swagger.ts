import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { INestApplication } from '@nestjs/common';

export const initSwagger = (app: INestApplication) => {
  const swaggerConfig = new DocumentBuilder()
    .setTitle('HitsN API')
    .addBearerAuth()
    .setDescription(
      'Esta es una API Creada con NestJS.',
    )
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api/docs', app, document, {
      explorer: true,
      swaggerOptions: {
          filter:true,
          showRequestDuration: true
      }
  });
};